# Global var
blockchain = []
open_transaction = []
owner = 'Max'


def get_last_blockchain_value():
    # -1: Last element
    if len(blockchain) < 1:
        return None
    return blockchain[-1]


def add_transaction(recipient, sender=owner, amount=1.0):
    '''
    Arguments:
      :sender: The sender of the coins
      :receipient: The recipient of the coins.
      :amount: The amount of coins sent with the transaction (default = 1.0)
    '''
    # dictionary=> {'key': value}. in this case, value is the arguments.
    transaction = {
        'sender': sender,
        'receipient': recipient,
        'amount': amount
    }
    open_transaction.append(transaction)


def mine_block():
    pass


def get_transaction_value():
    tx_receipient = input('Enter the recipient of the transaction:')
    tx_amount = float(input("Your transaction amount please:"))
    return tx_receipient, tx_amount


def get_user_choice():
    user_input = input("Your choice:")
    return user_input


def print_blockchain_elements():
    # Output the blockchain list to console
    for block in blockchain:
        print("Outputting block")
        print(block)
    else:
        print("-" * 20)


def verify_chain():
    # block_index = 0
    is_valid = True
    for block_index in range(len(blockchain)):
        if block_index == 0:
            continue
        elif blockchain[block_index][0] == blockchain[block_index - 1]:
            is_valid = True
            # print(block[0])
            # print(blockchain[block_index - 1])
        else:
            is_valid = False
            break
    # for block in blockchain:
    # if block_index == 0:
    #     block_index += 1
    #     continue
    # elif block[0] == blockchain[block_index - 1]:
    #     is_valid = True
    #     # print(block[0])
    #     # print(blockchain[block_index - 1])
    # else:
    #     is_valid = False
    #     break
    # block_index += 1
    return is_valid


'''
# Get the first transaction input and add the value to the Blockchain
tx_amount = get_transaction_value()
add_transaction(tx_amount)
'''

waiting_for_input = True

while waiting_for_input:
    print("Please choose")
    print("1. Add a new TX value")
    print("2. Output the blockchain blocks")
    print("h. Manupulate the chain")
    print('q: quit')

    user_choise = get_user_choice()
    if user_choise == '1':
        tx_data = get_transaction_value()
        recipient, amount = tx_data
        # amount=amount: Ommit sender
        add_transaction(recipient, amount=amount)
        print(open_transaction)
    elif user_choise == '2':
        print_blockchain_elements()
    elif user_choise == 'h':
        if len(blockchain) >= 1:
            blockchain[0] = [2]
    elif user_choise == 'q':
        waiting_for_input = False
    else:
        print("Input was invalid, please pick a value from the list!")
    if not verify_chain():
        print_blockchain_elements()
        print("Invalid blockchain")
        break
# When loop done
else:
    print("User left")

print("Done!")


"""
* array_name.append(add things)
* Local var: defined in def. Cannot call from outside of def.
* Global var: defined in outside of def. Can call from anywhere.
* if not verify_chain():  NOT means " if false " .

* Manupulating
Your transaction amount please:2
Please choose
1. Add a new TX value
2. Output the blockchain blocks
h. Manupulate the chain
q: quit
Your choice:1
Your transaction amount please:5.5
Please choose
1. Add a new TX value
2. Output the blockchain blocks 
h. Manupulate the chain
q: quit
Your choice:2
Outputting block
[[1], 2.0]
Outputting block
[[[1], 2.0], 5.5]
Please choose
1. Add a new TX value
2. Output the blockchain blocks
h. Manupulate the chain
q: quit
Your choice:h
Please choose
1. Add a new TX value
2. Output the blockchain blocks
h. Manupulate the chain
q: quit
Your choice:2
Outputting block
[2] <<HERE!!!!! This should [[1], 2.0] but manupulated!!
Outputting block
[[[1], 2.0], 5.5]
"""
